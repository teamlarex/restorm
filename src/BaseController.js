/**
 * Created by tthlex on 16/01/16.
 */

var fs = require('fs');
var path = require('path');
var _ = require('lodash');

function loadFilesSync(filePath){
    var files = fs.readdirSync(filePath);
    var data = [];
    files.forEach(function(file){
        data.push(require(path.resolve(filePath, file)));
    });
    return data;
}

function RouteCreator(options, controllers){
    var routeInterface = {};
    RouterCreator.BASE = '/';

    //this is attached to all the controllers via name
    var superRouter = options.express.Router();
    controllers.forEach(function(controller){
        var controllerData = controller.call(null);
        var controllerRouter = options.express.Router()
            .get(controllerData.MOUNT_PATH, controllerData.handleGet)
            .post(controllerData.MOUNT_PATH, controllerData.handlePost)
            .delete(controllerData.MOUNT_PATH, controllerData.handleDelete)
            .put(controllerData.MOUNT_PATH, controllerData.handlePut);

        superRouter.use(RouterCreator.BASE, controllerRouter);
    });

    routeInterface.getSuperRoute = superRouter;
    return routeInterface;
}
function setupControllers(options, controllers, models, transforms){
    //initialize express router
    var RestormRouter = options.express.Router();
    RestormRouter.use(options.mountApi, RouteCreator(options, controllers).getSuperRoute);
    return RestormRouter;
};

/**
 *
 * - you need to pass in appRoot directory to tell restorm how to find your models, controller and transformations files
 *
 * */
module.exports = function(options){

    var defaultOptions = {};
    var appRoot = options.appRoot;

    //resolve default dirs
    defaultOptions.modelDir = path.resolve(appRoot, 'models');
    defaultOptions.transformDir = path.resolve(appRoot, 'transforms');
    defaultOptions.controllerDir = path.resolve(appRoot, 'controllers');

    //api path
    defaultOptions.mountApi = '/';

    //resolve options
    var useOptions = _.merge(defaultOptions, options);

    //load files
    var controllers = loadFilesSync(useOptions.controllerDir);
    var models = loadFilesSync(useOptions.modelDir);
    var transforms = loadFilesSync(useOptions.transformDir);

    //load controllers, models, and transformations
    return setupControllers.call(null, useOptions, controllers, models, transforms);
};
/**
 * Created by tthlex on 16/01/16.
 */
var controller = require('./BaseController');
var model = require('./BaseModel');
module.exports.init = function(options){
    return controller(options);
};
module.exports.Model = model;
module.exports.Controller = controller;